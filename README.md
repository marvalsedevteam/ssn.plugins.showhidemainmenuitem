# Plugin to show/hide items on main menu

## Information

	It is possible to only show certain main menu items

## Installation

	Please see your MSM documentation for information on how to install plugins.

## Dependence of MSM code

	- MenuController.addMenuItem
	- MenuController.onMenuItemClicked
	- MenuController._buildMenuFromMarkup

## Functions

### 1.0
	
	- Show and hide items which are configured in plugin setting "IndexOfPrimaryIcons".
		Example of IndexOfPrimaryIcons: 0;1;7

### 1.1
	- Menu Items based on user's role
	- Configuration is a json string like {'RoleId':'Index, index'}
		Example of IndexOfPrimaryIcons: { '0':'0,1,7', '1':'0,1,2,5,6,7', '3':'0,1,2,7'}

## Compatible Versions

| Plugin   | MSM			|
|----------|----------------|
| 1.0	   | 14.10			|
| 1.1	   | 14.10,14.11	|